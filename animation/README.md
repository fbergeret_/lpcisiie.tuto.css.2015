# Tutoriels CSS ANIMATION
 
## Auteurs :
	* Adrien Richard
	* Fabien Breuil
	* Victorien Schweitzer 

------------------------ 

## Définition / Rappel W3C
Les animations CSS permettent d'appliquer a un élément un changement visuel de facon graduée. C'est à dire permettre de modifier les valeurs de propriétés CSS plusieurs fois dans le temps en enchainant les transitions css. 
Cette fonction est encore expérimentale pusiqu'elle n'est pas implémentée dans tout les navigateurs :



------------------------ 

## Tutoriel

Les animations se décomposent en plusieurs propriétés, décrites ici :

    * animation-name : le nom de l’animation. Important !
    * animation-duration : le temps total de l’animation.
    * animation-timing-function : accélération, décélération.
    * animation-iteration-count : le nombre de répétition de l’animation.
    * animation-direction : normal/inverse.
    * animation-delay : le temps avant que l’animation démarre.
    * animation-fill-mode : conserver l’état de l’animation avant le début où après la fin de celle-ci.

Il existe aussi la propriété raccourcie animation.
Par xemple : animation: nomAnim 2s linear 8s;

Ensuite, créer votre animation : 

    @keyframes monanimation {
        from {
        	Valeurs de départ }
    	75% {
    	  	Nouvelles valeurs
    	}
        to {Valeurs de fin }
    }
Il est donc possible de gérer l'animation dans ses détails par rapport au temps de celle ci définit dans animation-duration.


Les animations permettent donc d'apporter un peu de vie a un site Web sans avoir à utiliser javaScript.

------------------------ 

## Implémentation dans les navigateurs :
 * Sans préfixes
 IE10, Firefox 16 et Opera 12.1 supportent les propriétés non-préfixées.

 * Avec préfixes :
 	* Chrome  : -webkit
 	* Firefox : 5.0 à < 16.0 -moz
 	* Opera	  : 12 à <12.1 -o
 	* Safari  : -webkit
 

------------------------ 

## Liens utiles :

Référence W3C : http://www.w3.org/TR/css3-animations/
Référence complete et exemples : https://developer.mozilla.org/fr/docs/Web/CSS/Animations_CSS
CMS animation :  https://daneden.github.io/animate.css/



