function ajoutEffet(choix, val) {
    // gray
    if (choix == 1) {
        document.getElementById('demo-image').style.filter = "grayscale(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "grayscale(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "grayscale(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "grayscale(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "grayscale(" + val + ")";

    }

    // blur
    else if (choix == 2) {
        document.getElementById('demo-image').style.filter = "blur(" + val + "px)";
        document.getElementById('demo-image').style.webkitFilter = "blur(" + val + "px)";
        document.getElementById('demo-image').style.mozFilter = "blur(" + val + "px)";
        document.getElementById('demo-image').style.msFilter = "blur(" + val + "px)";
        document.getElementById('demo-image').style.oFilter = "blur(" + val + "px)";
    }

    // contrast
    else if (choix == 3) {
        document.getElementById('demo-image').style.filter = "contrast(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "contrast(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "contrast(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "contrast(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "contrast(" + val + ")";
    }

    // hue rotate
    else if (choix == 4) {
        document.getElementById('demo-image').style.filter = "hue-rotate(" + val + "deg)";
        document.getElementById('demo-image').style.webkitFilter = "hue-rotate(" + val + "deg)";
        document.getElementById('demo-image').style.mozFilter = "hue-rotate(" + val + "deg)";
        document.getElementById('demo-image').style.msFilter = "hue-rotate(" + val + "deg)";
        document.getElementById('demo-image').style.oFilter = "hue-rotate(" + val + "deg)";
    }

    // invert
    else if (choix == 5) {
        document.getElementById('demo-image').style.filter = "invert(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "invert(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "invert(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "invert(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "invert(" + val + ")";
    }

    // opacity
    else if (choix == 6) {
        document.getElementById('demo-image').style.filter = "opacity(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "opacity(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "opacity(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "opacity(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "opacity(" + val + ")";
    }

    // sepia
    else if (choix == 7) {
        document.getElementById('demo-image').style.filter = "sepia(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "sepia(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "sepia(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "sepia(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "sepia(" + val + ")";
    }

    // saturate
    else if (choix == 8) {
        document.getElementById('demo-image').style.filter = "saturate(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "saturate(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "saturate(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "saturate(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "saturate(" + val + ")";
    }

    // brightness
    else if (choix == 9) {
        document.getElementById('demo-image').style.filter = "brightness(" + val + ")";
        document.getElementById('demo-image').style.webkitFilter = "brightness(" + val + ")";
        document.getElementById('demo-image').style.mozFilter = "brightness(" + val + ")";
        document.getElementById('demo-image').style.msFilter = "brightness(" + val + ")";
        document.getElementById('demo-image').style.oFilter = "brightness(" + val + ")";
    }
}

function reinit() {
    document.getElementById('demo-image').removeAttribute("style");
}